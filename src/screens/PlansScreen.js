import React, { useEffect, useState } from 'react'
import { addDoc, collection, doc, getDocs, onSnapshot, query, where } from "firebase/firestore";
import db from '../firebase'
import './PlansScreen.css'
import { useSelector } from 'react-redux';
import { selectUser } from '../features/userSlice';
import { loadStripe } from '@stripe/stripe-js';

function PlansScreen() {
    const [products, setProducts] = useState([]);
    const user = useSelector(selectUser);
    const [subscription, setSubscription] = useState(null);

    let getSubscriptionData = async () => {
        try {
            const docRef = doc(db, 'customers', (user.uid))
            const subscriptions = collection(docRef, 'subscriptions');
            const currentSubscription = await getDocs(subscriptions);

            currentSubscription.forEach(async (subscription) => {
                setSubscription({
                    role: subscription.data().role,
                    current_period_end: subscription.data().current_period_end.seconds,
                    current_period_start: subscription.data().current_period_start.seconds,
                })
            })

        } catch (err) {
            console.error(err)
        }
    }

    useEffect(() => {
        getSubscriptionData();
    }, [user.uid])

    // console.log('subscription', subscription);

    let getPlansData = async () => {
        try {
            const productsRef = query(collection(db, 'products'), where('active', '==', true));
            const allProducts = await getDocs(productsRef);
            const products = {};

            allProducts.forEach(async (productDoc) => {
                products[productDoc.id] = productDoc.data();
                const priceSnap = await getDocs(collection(productDoc.ref, 'prices'));
                priceSnap.docs.forEach((price) => {
                    products[productDoc.id].prices = {
                        priceId: price.id,
                        priceData: price.data()
                    }
                })
            });
            setProducts(products);
            return products;
        } catch (err) {
            console.error(err)
        }
    }

    useEffect(() => {
        getPlansData();
    }, [])

    // console.log('object 123', products);

    const loadCheckout = async (priceId) => {
        const docRef = doc(db, 'customers', (user.uid))
        const checkout_sessions = collection(docRef, 'checkout_sessions');
        const priceCollection = await addDoc(checkout_sessions, {
            price: priceId,
            success_url: window.location.origin,
            cancel_url: window.location.origin,
        })

        onSnapshot(priceCollection, async (snap) => {
            const { error, sessionId } = snap.data();

            if (error) {
                // show an error to your customer and
                // inspect your Cloud Function logs in the firebase console.
                alert(`An error occured:, ${error.message}`);
            }

            if (sessionId) {
                // we have a session, let's redirect to Checkout
                // init Stripe
                const stripe = await loadStripe('pk_test_51Lq5KJSGRyipoO4Gp8KTMhpveq76oluX4jrDVsqgx12ispiBktlxSsMVOUmh7KsAgExplFMONb7x3iWfzWdawYIq00MuOCx4t2');
                stripe.redirectToCheckout({ sessionId });
            }
        })

        // priceCollection.onSnapshot(async (snap) => {
        //     const { error, sessionId } = snap.data();

        //     if (error) {
        //         // show an error to your customer and
        //         // inspect your Cloud Function logs in the firebase console.
        //         alert(`An error occured:, ${error.message}`);
        //     }

        //     if (sessionId) {
        //         // we have a session, let's redirect to Checkout
        //         // init Stripe
        //         const stripe = await loadStripe('pk_test_51Lq5KJSGRyipoO4Gp8KTMhpveq76oluX4jrDVsqgx12ispiBktlxSsMVOUmh7KsAgExplFMONb7x3iWfzWdawYIq00MuOCx4t2');
        //         stripe.redirectToCheckout({ sessionId });
        //     }
        // })

        // console.log('first docRef', docRef);
        // console.log('first checkout_sessions', checkout_sessions);
    };

    return (
        <div className='plansScreen'>
            {subscription && (
                <p>Renewal Date: {new Date(subscription?.current_period_end * 1000).toLocaleDateString()}</p>
            )}
            {Object.entries(products).map(([productId, productData]) => {
                // TODO: add some logic to check if the user's subscription is active....
                const isCurrentPackage = productData.name?.toLowerCase().includes(subscription?.role)

                return (
                    <div className={`${isCurrentPackage && 'plansScreen__plan--disabled'} plansScreen__plan`} key={productId}>
                        <div className="planScreen__info">
                            <h5>{productData.name}</h5>
                            <h6>{productData.description}</h6>
                        </div>

                        <button
                            onClick={() => !isCurrentPackage && loadCheckout(productData.prices.priceId)}
                        >
                            {isCurrentPackage ? 'Current Package' : 'Subscribe'}
                        </button>
                    </div>
                );
            })}
        </div>
    )
}

export default PlansScreen