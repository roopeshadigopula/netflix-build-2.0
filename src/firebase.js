import { initializeApp } from 'firebase/app';
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth'
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyBtkauHfNzMa8ysSNvkSJ42nPCWJvOSO1E",
    authDomain: "netflix-clone-34b45.firebaseapp.com",
    projectId: "netflix-clone-34b45",
    storageBucket: "netflix-clone-34b45.appspot.com",
    messagingSenderId: "101870455022",
    appId: "1:101870455022:web:4606147b72d146a374cbff"
};

const firebaseApp = initializeApp(firebaseConfig);

const auth = getAuth(firebaseApp);
let createUser = (email, password) => {
    createUserWithEmailAndPassword(
        auth,
        email,
        password
    ).then((authUser) => {
        console.log(authUser);
    }).catch((error) => {
        console.error(error.message);
    });
}
let signInUser = (email, password) => {
    signInWithEmailAndPassword(
        auth,
        email,
        password
    ).then((authUser) => {
        console.log(authUser);
    }).catch((error) => {
        console.error(error.message);
    });
}

const db = getFirestore(firebaseApp);

export { auth, createUser, signInUser };
export default db;